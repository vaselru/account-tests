package com.accounts;

import com.accounts.balance.BalanceEntity;
import com.accounts.db.DbTester;
import com.accounts.entry.EntryEntity;
import com.accounts.account.AccountEntity;
import org.junit.Test;

public class DatabaseTest {
    @Test
    public void databaseTest() {
        DbTester.tableColumnTest("account", AccountEntity.class);
        DbTester.tableColumnTest("balance", BalanceEntity.class);
        DbTester.tableColumnTest("entry", EntryEntity.class);

    }
}
