package com.accounts.account;

import com.accounts.AbstractEntity;
import com.accounts.utils.DateUtils;
import org.springframework.jdbc.core.RowMapper;

import java.time.LocalDateTime;
import java.util.UUID;

public class AccountEntity extends AbstractEntity{
    private UUID id;
    private String name;
    private String type;
    private String currency;
    private Boolean active;
    private LocalDateTime created;
    private LocalDateTime updated;

    public AccountEntity(UUID id, String name, String type, String currency, Boolean active,
                         LocalDateTime created,LocalDateTime updated){
        this.id = id;
        this.name = name;
        this.type = type;
        this.currency = currency;
        this.active = active;
        this.created = created;
        this.updated = updated;
    }
    public static RowMapper<AccountEntity> mapper() {
        return (rs, rowNum) -> new AccountEntity(
                UUID.fromString(rs.getString("id")),
                rs.getString("name"),
                rs.getString("type"),
                rs.getString("currency"),
                rs.getBoolean("active"),
                DateUtils.timestampToLocalDateTime(rs.getTimestamp("created")),
                DateUtils.timestampToLocalDateTime(rs.getTimestamp("updated"))
        );
    }
    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getCurrency() {
        return currency;
    }

    public Boolean getActive() {
        return active;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public LocalDateTime getUpdated() {
        return updated;
    }
}
