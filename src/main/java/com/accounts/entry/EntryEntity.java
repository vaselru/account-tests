package com.accounts.entry;

import com.accounts.AbstractEntity;
import com.accounts.utils.DateUtils;
import com.accounts.utils.DbUtils;
import org.springframework.jdbc.core.RowMapper;

import java.time.LocalDateTime;
import java.util.UUID;

public class EntryEntity extends AbstractEntity {
    private UUID id;
    private UUID accountDebit;
    private UUID accountCredit;
    private LocalDateTime dateTime;
    private Double amount;
    private String extSystemCode;
    private String extDocCode;
    private String comment;

    public  EntryEntity(UUID id, UUID account_debit, UUID account_credit, LocalDateTime dateTime,
                        Double amount, String ext_system_code, String ext_doc_code, String comment){
        this.id = id;
        this.accountDebit = account_debit;
        this.accountCredit = account_credit;
        this.dateTime = dateTime;
        this.amount = amount;
        this.extSystemCode = ext_system_code;
        this.extDocCode = ext_doc_code;
        this.comment = comment;
    }

    public static RowMapper<EntryEntity> mapper() {
        return (rs, rowNum) -> new EntryEntity(
                UUID.fromString(rs.getString("id")),
                DbUtils.getUUID(rs, "account_debit"),
                DbUtils.getUUID(rs, "account_credit"),
                DateUtils.timestampToLocalDateTime(rs.getTimestamp("dateTime")),
                rs.getDouble("amount"),
                rs.getString("ext_system_code"),
                rs.getString("ext_doc_code"),
                rs.getString("comment")
        );
    }

    public UUID getId() {
        return id;
    }

    public UUID getAccountDebit() {
        return accountDebit;
    }

    public UUID getAccountCredit() { return accountCredit;}

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public Double getAmount() {
        return amount;
    }

    public String getExtSystemCode() {
        return extSystemCode;
    }

    public String getExtDocCode() { return extDocCode;}

    public String getComment() { return comment;}
}
