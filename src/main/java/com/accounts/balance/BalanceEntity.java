package com.accounts.balance;

import com.accounts.AbstractEntity;
import com.accounts.utils.DateUtils;
import com.accounts.utils.DbUtils;
import org.springframework.jdbc.core.RowMapper;

import java.time.LocalDate;
import java.util.UUID;


public class BalanceEntity extends AbstractEntity{
    private UUID id;
    private UUID accountId;
    private LocalDate date;
    private Double amount;

    public BalanceEntity(UUID id, UUID accountId, LocalDate date, Double amount){
        this.id = id;
        this.accountId = accountId;
        this.date = date;
        this.amount = amount;
    }
    public static RowMapper<BalanceEntity> mapper() {
        return (rs, rowNum) -> new BalanceEntity(
                UUID.fromString(rs.getString("id")),
                DbUtils.getUUID(rs, "account_id"),
                DateUtils.timestampToLocalDate(rs.getTimestamp("date")),
                rs.getDouble("amount")
        );
    }
    public UUID getId() {
        return id;
    }

    public UUID getAccount_id() {
        return accountId;
    }

    public LocalDate getDate() {
        return date;
    }

    public Double getAmount() {
        return amount;
    }
}
