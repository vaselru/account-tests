package com.accounts.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

public class DbUtils {
    public static UUID getUUID(ResultSet rs, String field){
        String str = null;
        try {
            str = rs.getString(field);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        if (str == null) return null;
        return UUID.fromString(str);
    }
}
