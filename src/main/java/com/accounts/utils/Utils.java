package com.accounts.utils;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

public class Utils {
    public static Config getConfig() {
        return ConfigFactory.load("application");
    }
}