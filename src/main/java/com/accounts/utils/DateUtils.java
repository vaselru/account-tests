package com.accounts.utils;

import java.sql.Date;
import java.sql.Timestamp;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.ThreadLocalRandom;

public class DateUtils {

    private static final DateTimeFormatter jsonDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSSSS");
    private static final DateTimeFormatter xmlDateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final DateTimeFormatter sqlFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS");
    private static final ZoneOffset zone = ZoneOffset.ofHours(3);

    public static Long dateTimeToLong(LocalDateTime dateTime) {
        return dateTime.toInstant(zone).toEpochMilli();
    }

    public static Long dateToLong(LocalDate date) {
        return dateTimeToLong(date.atStartOfDay());
    }

    public static LocalDateTime longToDateTime(Long l) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(l), zone);
    }

    public static Date localDateToDate(LocalDate date) {
        return Date.valueOf(date);
    }

    public static Timestamp localDateTimeToTimestamp(LocalDateTime dateTime) {
        if (dateTime == null) return null;
        return new Timestamp(dateTimeToLong(dateTime));
    }

    public static String localDateTimeToXml(LocalDateTime dateTime) {
        return dateTime.format(jsonDateTimeFormatter);
    }

    public static String localDateToXml(LocalDate date) {
        return date.format(xmlDateFormatter);
    }

    public static Timestamp stringToTimestamp(String dateTimeString) {
        return localDateTimeToTimestamp(LocalDateTime.parse(dateTimeString, sqlFormatter));
    }

    public static LocalDateTime stringToDateTime(String dateString) {
        return LocalDateTime.parse(dateString, jsonDateTimeFormatter);
    }

    public static LocalDateTime timestampToLocalDateTime(Timestamp timestamp) {
        if (timestamp == null) return null;
        return timestamp.toLocalDateTime();
    }

    public static LocalDate timestampToLocalDate(Timestamp timestamp) {
        if (timestamp == null) return null;
        return timestampToLocalDateTime(timestamp).toLocalDate();
    }

    public static long between(LocalDateTime dateTime, LocalDateTime dateTime1) {
        return Duration.between(dateTime, dateTime1).getSeconds();
    }

    public static int betweenDays(LocalDate dateTime, LocalDate dateTime1) {
        return (int) Duration.between(dateTime.atStartOfDay(), dateTime1.atStartOfDay()).toDays();
    }

    public static LocalDate getRndDate() {
        long minDay = LocalDate.now().minusYears(1).toEpochDay();
        long maxDay = LocalDate.of(2100, 12, 31).toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    public static LocalDateTime getNow() {
        LocalDateTime now = LocalDateTime.now().minusSeconds(1);
        System.out.println("now = " + now.toString());
        return now;
    }
}
