package com.accounts;

import com.accounts.db.DbTester;
import org.springframework.jdbc.core.JdbcTemplate;

public abstract class AbstractDAO {
    private final JdbcTemplate jdbcTemplate;

    public AbstractDAO() {
        this.jdbcTemplate = DbTester.getJdbcTemplate();
    }

    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }
}
