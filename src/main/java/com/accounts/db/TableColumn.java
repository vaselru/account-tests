package com.accounts.db;

import org.springframework.jdbc.core.RowMapper;

public class TableColumn {
    private String columnName;
    private String dataType;

    public TableColumn(String columnName, String dataType) {
        this.columnName = columnName.replaceAll("_", "");
        this.dataType = dataType;
    }

    public static RowMapper<TableColumn> mapper() {
        return (rs, rowNum) -> new TableColumn(
                rs.getString("column_name"),
                rs.getString("data_type")
        );
    }

    public String getColumnName() {
        return columnName;
    }
}