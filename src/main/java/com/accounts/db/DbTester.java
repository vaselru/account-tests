package com.accounts.db;

import com.accounts.AbstractEntity;
import com.accounts.utils.Utils;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import com.typesafe.config.Config;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class DbTester {
    public static JdbcTemplate jdbcTemplate;

    public static JdbcTemplate getJdbcTemplate() {
        if (jdbcTemplate != null) return jdbcTemplate;
        DataSource dataSource = createDataSource(Utils.getConfig().getConfig("datasource"));
        jdbcTemplate = new JdbcTemplate(dataSource);
        deleteTestData();
        return jdbcTemplate;
    }

    private static HikariDataSource createDataSource(Config config) {
        final HikariDataSource ds = new HikariDataSource();
        ds.setJdbcUrl(config.getString("url"));
        ds.setUsername(config.getString("username"));
        ds.setPassword(config.getString("password"));
        ds.setDriverClassName(config.getString("driverClassName"));
        ds.setAutoCommit(config.getBoolean("defaultAutoCommit"));
        ds.setMaximumPoolSize(config.getInt("hikari.maximumPoolSize"));
        ds.setMinimumIdle(config.getInt("hikari.minimumIdle"));
        ds.setConnectionTestQuery(config.getString("hikari.connectionTestQuery"));
        ds.setTransactionIsolation(config.getString("hikari.transactionIsolation"));
        ds.setSchema("public");
        return ds;
    }

    public static void tableColumnTest(String tableName, Class<? extends AbstractEntity> klass) {
        List<TableColumn> columnNames = new TableColumnDAO().getColumns(tableName);
        List<String> fieldNames = new ArrayList<>();
        Arrays.stream(klass.getDeclaredFields()).forEach(f -> fieldNames.add(f.getName().toLowerCase()));
        columnNames.forEach(col ->
                assertThat(fieldNames.stream().anyMatch(f -> f.equalsIgnoreCase(col.getColumnName()))).as(
                        String.format("Не найден столбец %s таблицы %s", col.getColumnName(), tableName)).isTrue()
        );
    }

    public static void deleteTestData(){

    }
}
