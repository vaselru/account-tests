package com.accounts.db;
import com.accounts.AbstractDAO;

import java.util.List;

public class TableColumnDAO extends AbstractDAO {
    public List<TableColumn> getColumns(String table) {
        return getJdbcTemplate().query(
                String.format("select column_name,data_type from information_schema.columns " +
                        "where table_name = '%s'", table), TableColumn.mapper()
        );
    }
}
